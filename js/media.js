// Uploading files
var file_frame;

jQuery('.upload-image-button').live('click', function( event ){

  event.preventDefault();

  var button = jQuery(event.target);
  var hidden = button.siblings('input');
  var img = button.siblings('img')

  // If the media frame already exists, reopen it.
  if ( file_frame ) {
    file_frame.open();
    return;
  }

  // Create the media frame.
  file_frame = wp.media.frames.file_frame = wp.media({
    title: jQuery( this ).data( 'uploader_title' ),
    button: {
      text: jQuery( this ).data( 'uploader_button_text' ),
    },
    multiple: false  // Set to true to allow multiple files to be selected
  });

  // When an image is selected, run a callback.
  file_frame.on( 'select', function() {
    // We set multiple to false so only get one image from the uploader
    attachment = file_frame.state().get('selection').first().toJSON();

    img.attr('src', attachment.url);
    hidden.val(attachment.id);
  });

  // Finally, open the modal
  file_frame.open();
});