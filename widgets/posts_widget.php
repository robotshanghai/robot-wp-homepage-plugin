<?php
/**
 * Adds Post_Widget widget.
 */
class Posts_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'posts_widget', // Base ID
			__( 'Posts Widget', 'text_domain' ), // Name
			array( 'description' => __( 'Choose posts to display.' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {

     	echo $args['before_widget'];
		if ( ! empty( $instance['widget_title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['widget_title'] ) . $args['after_title'];
		}
		
		$postLoop = $this->getPosts($instance);

		if( $postLoop->have_posts() ){
			while( $postLoop->have_posts() ){
				$postLoop->the_post(); ?>
<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
	<h5><?php the_title(); ?></h5>
	<?php if( has_post_thumbnail() ) { ?><div class="thumb"><?php the_post_thumbnail(); ?></div><?php } ?>
	<div class="content"><?php the_content(); ?></div>
</a>
		<?php }
		}
		else{
			echo __('No posts chosen.');
		}

		wp_reset_postdata();
		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		// queue up the javascript
	    //wp_enqueue_script( 'robot-custom-homepage', plugins_url('/js/robot-homepage.js', __FILE__), array('jquery', 'bootstrap') );
	    //wp_localize_script( 'robot-custom-homepage', 'ajax_object', array('ajaxUrl' => admin_url('admin-ajax.php')) );

     	$widgetTitle = ! empty( $instance['widget_title'] ) ? $instance['widget_title'] : '';
     	$postType = ! empty( $instance['post_type'] ) ? $instance['post_type'] : 'post';
		?>
<p>
	<input class="widefat" id="<?php echo $this->get_field_id( 'widget_title' ); ?>"
		name="<?php echo $this->get_field_name( 'widget_title' ); ?>"
		type="text" value="<?php echo esc_attr( $widgetTitle ); ?>"
		placeholder="<?php echo __('Section Title'); ?>">
	<input class="widefat post-type" id="<?php echo $this->get_field_id( 'post_type' ); ?>"
		name="<?php echo $this->get_field_name( 'post_type' ); ?>"
		type="text" value="<?php echo esc_attr( $postType ); ?>"
		placeholder="<?php echo __('Post Type (post, product, <custom>)'); ?>">
	<input class="widefat robot-homepage-posts-typeahead"
		id="rhp-typeahead-<?php echo $this->id; ?>"
		placeholder="<?php echo __('Typeahead...'); ?>">
	<div id="rhp-container-<?php echo $this->id; ?>">
	<?php $postLoop = $this->getPosts( $instance );
	if( $postLoop != null && $postLoop->have_posts() ):
		$count = 0;
		while( $postLoop->have_posts() ): $postLoop->the_post(); ?>
		<input value="<?php echo get_the_ID(); ?>" type="checkbox" checked="checked"
			name="<?php echo $this->get_field_name( 'post_ids' ); ?>[]"
			id="rph-item-<?php echo "$this->id-$count"; ?>">
		<label for="rph-item-<?php echo "$this->id-$count"; ?>"><?php the_title(); ?></label>
		<br>
	<?php $count++; endwhile; endif; ?>
	</div>
	<script type="text/javascript">//<![CDATA[
	(function($){
		var baseId = '<?php echo $this->id; ?>';
		var inputNameBase = '<?php echo $this->get_field_name( 'post_ids' ); ?>';
		var inputBox = $('#rhp-typeahead-'+baseId);
		var dataType = inputBox.attr('data-type');
		var checkContainer = $('#rhp-container-'+baseId);
		var typeInput = $('#<?php echo $this->get_field_id( 'post_type' ); ?>');
		inputBox.typeahead({
			minLength: 3,
			highlight: true,
			hint: true,
		},{
			source : function(q, cb){
				$.getJSON('<?php echo admin_url('admin-ajax.php'); ?>', {
					'action' : 'post-search',
					'title' : q,
					'type' : typeInput.val(),
				})
				.done(function( data, textStatus, jqXHR ) {
					cb(data['data']);
				})
				.fail(function( jqXHR, textStatus, errorThrown ) {
					cb([]);
					alert('Please enter a post type.');
				});
			},
			name : 'posts',
			displayKey : 'title',
		}).on('typeahead:selected', function(obj, data){
			var inputs = checkContainer.children('input');
			var found = false;
			var count = inputs.length;
			for (var i = count - 1; i >= 0; i--) {
				if(inputs[i].value == data.id){
					found = true;
					inputs[i].checked = "checked";
				}
			}

			if(!found){
				checkContainer.append('<input value="'+data.id+'" type="checkbox" '+
					'checked="checked" name="'+inputNameBase+'[]" id="rph-item-'+baseId+'-'+count+'">'+
					'<label for="rph-item-'+baseId+'-'+count+'">'+data.title+'</label><br>');
			}

			inputBox.val('');
		});
	})(jQuery)
	//]]></script>
</p><?php
	} // END OF FORM FUNCTION

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {

		$instance = array();
		$instance['post_ids'] = array();
		$instance['widget_title'] = ( ! empty( $new_instance['widget_title'] ) ) ? strip_tags( $new_instance['widget_title'] ) : '';
		$instance['post_type'] = ( ! empty( $new_instance['post_type'] ) ) ? strip_tags( $new_instance['post_type'] ) : '';

		try{
			foreach ($new_instance['post_ids'] as $value) {
				$instance['post_ids'][] = (int) $value;
			}
		}
		catch(Exception $e){
			// we don't really care if it didn't exist or if it was invalid
			// whatever caused the exception, we already have an empty array.
		}
		
		return $instance;
	}

	/**
	 * Utillity function to get the posts from the instance
	 *
	 * @param array Previously saved values from database.
	 *
	 * @return WPLoop WordPress Loop of posts
	 */
	private function getPosts( $instance ) {
		if( empty( $instance['post_ids'] ) ){
			return null;
		}

		return new WP_Query( array(
			'post__in' => $instance['post_ids'],
			'post_type' => $instance['post_type'],
		) );
	}
}