<?php
require_once( dirname(__FILE__) . '/category_widget.php' );

class Tag_Widget extends Category_Widget {
	protected static $taxonomy = 'tag';
	protected static $title = 'Tag';
}