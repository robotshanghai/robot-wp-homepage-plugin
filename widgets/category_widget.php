<?php
/**
 * Adds Category_Widget widget.
 */
class Category_Widget extends WP_Widget {

	protected static $taxonomy = 'category';
	protected static $title = 'Category';

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			static::$taxonomy.'_widget', // Base ID
			__( static::$title.' List', 'text_domain' ), // Name
			array( 'description' => __( 'Display posts from a '.static::$taxonomy, 'text_domain' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
	
     	echo $args['before_widget'];
		if ( ! empty( $instance['taxonomy_name'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['taxonomy_name'] ). $args['after_title'];
		}
		
		$my_query = new WP_Query( array(
			'cat' => $instance['taxonomy_id'],
			'posts_per_page' => $instance['taxonomy_count'] == 0 ? -1 : $instance['taxonomy_count'],
		) );

		if( $my_query->have_posts() ){
			while( $my_query->have_posts() ){
				$my_query->the_post(); ?>
<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
	<h5><?php the_title(); ?></h5>
	<?php if( has_post_thumbnail() ): ?><div class="thumb"><?php the_post_thumbnail(); ?></div><?php endif; ?>
	<div class="content"><?php the_content(); ?></div>
</a>
		<?php }
		}
		else{
			echo __('This category is empty...');
		}

		wp_reset_postdata();
		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
     	$categoryName = ! empty( $instance['taxonomy_name'] ) ? $instance['taxonomy_name'] : '';
		$categoryId = ! empty( $instance['taxonomy_id'] ) ? $instance['taxonomy_id'] : 1;
		$categoryCount = empty( $instance['taxonomy_count'] ) ? '' : $instance['taxonomy_count'];
		?>
	<p>
		<input class="widefat" id="<?php echo $this->get_field_id( 'taxonomy_name' ); ?>" name="<?php echo $this->get_field_name( 'taxonomy_name' ); ?>" type="text" value="<?php echo esc_attr( $categoryName ); ?>" placeholder="<?php echo __('Section Title'); ?>">
		<input class="widefat" id="<?php echo $this->get_field_id( 'taxonomy_count' ); ?>" name="<?php echo $this->get_field_name( 'taxonomy_count' ); ?>" type="number" value="<?php echo esc_attr( $categoryCount ); ?>" placeholder="<?php echo __('Maximum (0 or empty for no max)'); ?>">
		<?php wp_dropdown_categories( array(
			'taxonomy' => static::$taxonomy,
			'name' => $this->get_field_name( 'taxonomy_id' ),
			'hide_empty' => 0,
			'id' => $this->get_field_id( 'taxonomy_id' ),
			'selected' => $categoryId,
		) ); ?>
	</p><?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['taxonomy_name'] = ( ! empty( $new_instance['taxonomy_name'] ) ) ? strip_tags( $new_instance['taxonomy_name'] ) : '';
		$instance['taxonomy_id'] = $new_instance['taxonomy_id'];

		try{
			$instance['taxonomy_count'] = (int) $new_instance['taxonomy_count'];
		}
		catch(Exception $e){
			// we don't really care if it didn't exist or if it was invalid
			// whatever caused the exception, we'll just set it to the default of 0
			$instance['taxonomy_count'] = 0;
		}

		return $instance;
	}

} // class Category_Widget