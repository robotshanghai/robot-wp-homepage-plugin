<?php
/*
Plugin Name: Robot Custom homepage
Plugin URI: http://wordpress.org/extend/plugins/robot-custom-homepage/
Description: A plugin for configuring homepage options that can be accessed via templates...
Author: Kevin Sweeney
Author URI: http://www.robotshanghai.com
Version: 0.1.0
Text Domain: robot-custom-hompage
License: GPL version 2 or later - http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
*/
defined('ABSPATH') or die("No script kiddies please!");

/* IMPORT WIDGET CLASSES! */
require( dirname(__FILE__) . '/widgets/category_widget.php' );
require( dirname(__FILE__) . '/widgets/tag_widget.php' );
require( dirname(__FILE__) . '/widgets/posts_widget.php' );

// import theme specific option file
include get_stylesheet_directory() . '/admin/robot-homepage.php';

/* REGISTER SCRIPTS! */
add_action( 'admin_enqueue_scripts', 'robot_admin_scripts' );
function robot_admin_scripts( $hook ){

	wp_enqueue_style( 'robot-style', plugin_dir_url(__FILE__) . '/css/robot.css' );

	if( 'appearance_page_robot-custom-homepage' == $hook ){
		wp_enqueue_media();
		wp_enqueue_script( 'robot-homepage-admin', plugin_dir_url(__FILE__) . '/js/media.js' );
	}

	if( 'widgets.php' == $hook ){
		wp_enqueue_script(
			'robot-typeahead',
			'https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.10.5/typeahead.jquery.min.js',
			array('jquery'), false );
	}
}

/* SETUP ADMIN PANELS */
add_action('admin_menu', 'robot_custom_homepage_add_menu_option');
function robot_custom_homepage_add_menu_option(){
	
	add_theme_page(	  __('Homepage Header'),
					  __('Homepage'),
					  'manage_options',
					  'robot-custom-homepage',
					  'robot_custom_homepage' );
}

function robot_custom_homepage(){
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}

	if( in_array( get_stylesheet_directory() . '/admin/robot-homepage.php', get_included_files() ) ): // the theme has options configured! print them... ?>

	<div class="wrap">
		<h2><?php echo __('Robot Homepage'); ?></h2>
		<p>
			<?php echo __('Here you can configure the homepage. You can also select homepage widgets: '); ?>
			<a href="<?php echo admin_url('widgets.php'); ?>"><?php echo __('Widgets'); ?></a>.
		</p>
		<form method="post" action="options.php">
		<?php settings_fields( 'robot_header_group' ); do_settings_sections( 'robot_header_section' ); ?>
		<input name="Submit" type="submit" value="<?php esc_attr_e('Save Changes'); ?>" />
		</form>
	</div>

	<?php else: // could not find theme options. alert that they should configure it... ?>
	<div><?php echo __('This page must be configured via the theme by creating a file called "robot-homepage.php".'); ?></div>
	<?php endif;
}

/* REGISTER THE WIDGETS SIDEBAR */
register_sidebar(array(  
    'name' => __('Homepage'),
    'id'   => __('homepage_widgets'),
    'description'   => __('Widget area for the homepage'),
    'before_widget' => '<div id="%1$s" class="homepage widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h4>',
    'after_title'   => '</h4>',
));

/* REGISTER THE WIDGETS */
add_action( 'widgets_init', function(){
    register_widget( 'Category_Widget' );
    register_widget( 'Tag_Widget' );
    register_widget( 'Posts_Widget' );
});

add_action( 'wp_ajax_post-search', function(){
	global $wpdb;
	
	if( !array_key_exists( 'title', $_GET ) || !array_key_exists( 'type', $_GET ) ){
		wp_send_json_error('Must specify "title" to search and a post "type".'); // dies
	}

	if( !$_GET['title'] || !$_GET['type'] ){
		wp_send_json_error('Title and post type must not be empty.'); // dies
	}
	
	$return = array();
	$results = $wpdb->get_results( $wpdb->prepare(
		"SELECT ID, post_title FROM $wpdb->posts
		 WHERE post_title LIKE %s AND post_type = %s
		 ORDER BY post_title DESC",
		'%' . $wpdb->esc_like( $_GET['title'] ) . '%',
		$_GET['type'] ) );

	foreach( $results as $result ) {
		$return[] = array(
			'id' => $result->ID,
			'title' => $result->post_title,
			'thumb' => has_post_thumbnail($result->ID) ? get_the_post_thumbnail($result->ID) : null,
		);
	}

	wp_send_json_success($return);
});